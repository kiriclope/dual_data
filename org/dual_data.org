#+TITLE: Dual Task Data Analysis
##+PROPERTY: header-args:ipython :results raw drawer :exports both :async t :session dual_data :kernel dual_data

* A few imports
#+begin_src ipython :results output :async t :session dual_data :kernel dual_data
  %load_ext autoreload
  %autoreload 2
  %reload_ext autoreload
#+end_src

#+RESULTS:
: The autoreload extension is already loaded. To reload it, use:
:   %reload_ext autoreload

#+begin_src ipython :results output :async t :session dual_data :kernel dual_data
  import os
  import sys

  sys.path.insert(0, '../dual_task')
  current_dir = os.path.dirname(os.path.abspath('__file__'))
  # Get parent directory (= the project root)
  project_root = os.path.join(current_dir, '..')
  # Append to system path
  sys.path.append(project_root)

  print("Python exe")
  print(sys.executable)

#+end_src

#+RESULTS:
: Python exe
: /home/leon/mambaforge/envs/dual_data/bin/python

#+begin_src ipython :results output :async t :session dual_data :kernel dual_data
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    import seaborn as sns
    import pandas as pd
    from dual_data.common.plot_utils import plot_summary

    sns.set_context("poster")
    sns.set_style("ticks")
    plt.rc("axes.spines", top=False, right=False)
    fig_path = '../figs/perf'
    golden_ratio = (5**.5 - 1) / 2
    width = 6

    matplotlib.rcParams['figure.figsize'] = [width, width * golden_ratio ]
    matplotlib.rcParams['lines.markersize'] = 6
    %matplotlib inline
    %config InlineBackend.figure_format = 'png'
#+end_src

#+RESULTS:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  print('a test figure')
  plt.figure()
  plt.plot([1,2,3,4], '-o')
  plt.xlabel('x')
  plt.show()
#+end_src

#+RESULTS:
:results:
# Out[10]:
[[file:./obipy-resources/zVhEzm.png]]
:end:

* Behavioral performance
*** Imports
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  from dual_data.performance.perf_tasks import run_perf_tasks
#+end_src

#+RESULTS:
:results:
# Out[13]:
:end:

*** single mouse
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_perf_tasks(mouse='JawsM15', perf_type='correct', reload=0)
#+end_src

#+RESULTS:
:results:
0 - a489fbcc-b9d3-422c-a02e-5453a8d98dd5
:end:

*** all mice

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mice = ['ChRM04','JawsM15', 'JawsM18', 'ACCM03', 'ACCM04', 'AP02', 'AP12']
  for mouse in mice:
      run_perf_tasks(mouse=mouse, perf_type='correct', reload=0)
      plt.close('all')
#+end_src

#+RESULTS:
:results:
# Out[19]:
:end:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_perf_tasks(mouse='all', perf_type='correct', reload=0)
#+end_src

#+RESULTS:
:results:
# Out[14]:
[[file:./obipy-resources/BRwTr7.png]]
:end:

*** summary
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  import pickle as pkl
  from dual_data.common.plot_utils import concat_fig
#+end_src

#+RESULTS:
:results:
# Out[20]:
:end:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mice = ['ChRM04','JawsM15', 'JawsM18', 'ACCM03', 'ACCM04', 'AP02', 'AP12']

  files = ['../figs/' + i + '_behavior_tasks_correct.pkl' for i in mice]
  print(files)

  figlist = [pkl.load(open(file, "rb")) for file in files]
  print(figlist)
  plt.close('all')

  golden_ratio = (5**.5 - 1) / 2
  width = 4.25
  height = width * golden_ratio * 1.2
  figsize = [width, height]

  concat_fig('summary', figlist, dim=[3, 3], size=figsize, VLINE=0)

#+end_src

#+RESULTS:
:results:
# Out[21]:
[[file:./obipy-resources/nN2lBI.png]]
:end:

* Licktime analysis
*** Imports
#+begin_src ipython :results raw drawer :exports both
  from scipy.io import loadmat
  from dual_data.licks.licks import *
#+end_src

#+RESULTS:
:results:
# Out[20]:
:end:

*** Data
#+begin_src ipython :results output
  # path = '../data/behavior/DualTask_DPA_vs_Single_DPA/'
  path = '../data/behavior/DualTask-Silencing-ACC-Prl/'
  # path = '../data/behavior/DualTask-Silencing-Prl-ACC/'
  # path = '../data/behavior/DualTask-Silencing-ACC/'
#+end_src

#+RESULTS:

*** Single mouse
#+begin_src ipython :results raw drawer :exports both
  i_mouse = 2
  i_session = 1

  if 'DPA' in path:
      session = 'Dual' # control opto DPA or Dual
  else:
      session = 'control' # control opto DPA or Dual

  data = loadmat(path + session + '_mouse_%d/session_%d' % (i_mouse, i_session))
#+end_src

#+begin_src ipython :results raw drawer :exports both
  licks_dpa, licks_go, licks_nogo = get_licks_mouse(data, path, response='correct', trial_length=20, verbose=1)
  licks_all = [np.hstack(licks_dpa), np.hstack(licks_go), np.hstack(licks_nogo)]
  licks_density, bins = plot_licks_hist(licks_all, n_bins='auto')
#+end_src

#+RESULTS:
:results:
# Out[31]:
[[file:./obipy-resources/Hutbpp.png]]
:end:

*** All mice
#+begin_src ipython :results raw drawer :exports both
mice_dpa, mice_go, mice_nogo = get_licks_mice(path, n_session=11, response="incorrect")
#+end_src

#+RESULTS:
:results:
# Out[40]:
:end:

**** control
#+begin_src ipython :results raw drawer :exports both
  n_mice = 9
  dpa_all = hstack_with_padding(mice_dpa[:n_mice])
  go_all = hstack_with_padding(mice_go[:n_mice])
  nogo_all = hstack_with_padding(mice_nogo[:n_mice])

  licks_all = [ np.hstack(dpa_all), np.hstack(go_all), np.hstack(nogo_all)]
  licks_density, bins = plot_licks_hist(licks_all, n_bins='auto', n_mice=n_mice)
#+end_src

#+RESULTS:
:results:
# Out[41]:
[[file:./obipy-resources/9UfRSc.png]]
:end:

**** opto
#+begin_src ipython :results raw drawer :exports both
  dpa_all = hstack_with_padding(mice_dpa[n_mice:])
  go_all = hstack_with_padding(mice_go[n_mice:])
  nogo_all = hstack_with_padding(mice_nogo[n_mice:])

  licks_all = [ np.hstack(dpa_all), np.hstack(go_all), np.hstack(nogo_all)]
  licks_density, bins = plot_licks_hist(licks_all, n_bins='auto', n_mice=n_mice)
#+end_src

#+RESULTS:
:results:
# Out[42]:
[[file:./obipy-resources/xbd4s3.png]]
:end:

* Temporal decoding
*** Imports
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data :kernel dual_data
  from dual_data.decode.mne_scores import run_mne_scores
  from dual_data.decode.mne_cross_temp import run_mne_cross_temp
#+end_src

#+RESULTS:
:results:
# Out[45]:
:end:

*** Sample classification
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_mne_scores(features='sample', task='DPA', day='first')
#+end_src

#+RESULTS:
:results:
# Out[8]:
[[file:./obipy-resources/rdJsvQ.png]]
:end:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_mne_cross_temp(features='sample', task='DPA', day='first')
#+end_src

#+RESULTS:
:results:
# Out[34]:
[[file:./obipy-resources/RNBphi.png]]
:end:
*** Distractor classification
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_mne_scores(features='distractor', task='Dual', day='first')
  run_mne_scores(features='distractor', task='Dual', day='last')
#+end_src

#+RESULTS:
:results:
# Out[103]:
[[file:./obipy-resources/o1QDkg.png]]
:end:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_mne_cross_temp(features='distractor', task='Dual', day='first')
#+end_src

#+RESULTS:
:results:
# Out[37]:
[[file:./obipy-resources/JZg9RA.png]]
:end:
*** Choice decoding
**** single mouse
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mouse='ChRM04'
  run_mne_scores(mouse=mouse, features='choice', task='Dual', day='first', bootstrap=0, balance=1)
#+end_src

#+RESULTS:
:results:
# Out[63]:
[[file:./obipy-resources/sG8jy0.png]]
:end:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_mne_scores(mouse=mouse, features='choice', task='Dual', day='last', laser=0, balance=1)
#+end_src

#+RESULTS:
:results:
# Out[64]:
[[file:./obipy-resources/BDTeB0.png]]
:end:

**** all mice
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mice = ['ChRM04','JawsM15', 'JawsM18', 'ACCM03', 'ACCM04']
  tasks = ['DPA', 'DualGo', 'DualNoGo', 'Dual']
  for mouse in mice:
      for task in tasks:
          run_mne_scores(mouse=mouse, features='choice', task=task, day='first', bootstrap=1)
          run_mne_scores(mouse=mouse, features='choice', task=task, day='last', bootstrap=1)
          plt.close('all')
#+end_src

* Overlaps
*** Imports
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data :kernel dual_data
  from dual_data.overlap.get_overlap import run_get_overlap
#+end_src

#+RESULTS:
:results:
# Out[5]:
:end:

*** Sample Overlap
**** single mouse
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mouse = 'JawsM15'
  run_get_overlap(mouse=mouse, features='sample', task='all', day='first', method='bolasso')
  # run_get_overlap(mouse=mouse, features='sample', task='all', day='last', method='bolasso')
#+end_src

#+RESULTS:
:results:
0 - 4f5de9ae-14d4-4546-bfa7-233743d6edba
:end:
**** all mice
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mice = ['ChRM04', 'JawsM15', 'JawsM18', 'ACCM03', 'ACCM04']
  tasks = ['DPA', 'DualGo', 'DualNoGo']

  # mice = ['Ja']
  # tasks = ['DPA', 'DualGo', 'DualNoGo']

  for mouse in mice:
      for task in tasks:
          run_get_overlap(mouse=mouse, features='sample', task=task, day='first', method='bolasso')
          run_get_overlap(mouse=mouse, features='sample', task=task, day='last', method='bolasso')
          plt.close('all')
#+end_src

#+RESULTS:
:results:
# Out[53]:
:end:

**** summary
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  from dual_data.common.plot_utils import concat_fig
#+end_src

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  # mice = ['ChRM04', 'JawsM15', 'JawsM18', 'ACCM03', 'ACCM04']

  import pickle as pkl
  files = ['../figs/' + i + '_' + j + '_sample_overlap.pkl' for i in mice for j in tasks]
  print(files)

  figlist = [pkl.load(open(file, "rb")) for file in files]
  print(figlist)
  plt.close('all')

  golden_ratio = (5**.5 - 1) / 2
  width = 4.25
  height = width * golden_ratio * 1.2
  figsize = [width, height]

  concat_fig('summary', figlist, dim=[len(mice), len(tasks)], size=figsize)

#+end_src

#+RESULTS:
:results:
# Out[56]:
[[file:./obipy-resources/K81ZvB.png]]
:end:

*** Distractor overlap
**** single mouse
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mouse = 'ACCM03'
  run_get_overlap(mouse=mouse, features='distractor', task='DPA', day='first', method='bolasso', pval=0.05, balance=1)
  run_get_overlap(mouse=mouse, features='distractor', task='DPA', day='last', method='bolasso', pval=0.05, balance=1)
#+end_src

#+RESULTS:
:results:
0 - 3b7483a7-e56f-4612-80d2-47672114a6db
:end:

**** all mice
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mice = ['ChRM04','JawsM15', 'JawsM18', 'ACCM03', 'ACCM04']
  tasks = ['DPA', 'DualGo', 'DualNoGo']
  for mouse in mice:
      for task in tasks:
          run_get_overlap(mouse=mouse, features='distractor', task=task, day='first', method='bolasso')
          run_get_overlap(mouse=mouse, features='distractor', task=task, day='last', method='bolasso')
          plt.close('all')
#+end_src

#+RESULTS:
:results:
0 - d77e8cde-efce-408c-8e16-d2c0793448c1
:end:

**** summary
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  import pickle as pkl
  from dual_data.common.plot_utils import concat_fig
#+end_src

#+RESULTS:
:results:
# Out[9]:
:end:

#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  mice = ['ChRM04','JawsM15', 'JawsM18', 'ACCM03', 'ACCM04']
  tasks = ['DPA', 'DualGo', 'DualNoGo']

  files = ['../figs/' + i + '_' + j + '_distractor_overlap.pkl' for i in mice for j in tasks]
  print(files)

  figlist = [pkl.load(open(file, "rb")) for file in files]
  print(figlist)
  plt.close('all')

  golden_ratio = (5**.5 - 1) / 2
  width = 4.25
  height = width * golden_ratio * 1.2
  figsize = [width, height]

  concat_fig('summary', figlist, dim=[len(mice), len(tasks)], size=figsize)

#+end_src

#+RESULTS:
:results:
# Out[13]:
[[file:./obipy-resources/YcZbR5.png]]
:end:

* Representational Dynamics
*** Imports
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  from dual_data.overlap.get_cos_day import run_get_cos_day
#+end_src

#+RESULTS:
:results:
# Out[16]:
:end:

*** single mouse
#+begin_src ipython :results raw drawer :exports both :async t :session dual_data
  run_get_cos_day(mouse='JawsM15', method='bootstrap', balance=1, trials='correct')
#+end_src

#+RESULTS:
:results:
# Out[17]:
[[file:./obipy-resources/Pn3bYz.png]]
:end:

* Bump attractor Dynamics
